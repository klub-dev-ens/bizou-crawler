# Bizou-crawler

Crawler de dossiers pour [Bizou](http://positon.org/bizou/), notre serveur de
photos.

Permet de forcer Bizou à générer ses thumbnails. Attention, c'est lent.


## Fonctionnement

```bash
python3 crawl.py
```

Un cookie vous sera demandé. Il s'agit de votre cookie `auth_token` sur toute
page en `photos.cof.ens.fr`, et qui sert à vous authentifier auprès du
[CAS](https://cas.eleves.ens.fr).
