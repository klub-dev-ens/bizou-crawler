import urllib.request
import urllib.parse
import urllib.error
import sys

ENTRY_URL = "https://photos.cof.ens.fr/"


def getUrl(url, cookie):
    req = urllib.request.Request(url)
    req.add_header("Cookie", "auth_token={}".format(cookie))
    return urllib.request.urlopen(req).read().decode("utf-8")


def crawl(url, cookie):
    print("<{}>".format(url))
    try:
        page = getUrl(url, cookie)
    except urllib.error.HTTPError as exn:
        print("\tCannot fetch <{}>: {}".format(url, exn), file=sys.stderr)
        return

    page = page[page.find('<div id="folders">') : page.find('<div id="images">')]

    lastIdx = page.find("<a href=")
    while lastIdx >= 0:
        strBegin = page.find('"', lastIdx)
        strEnd = page.find('"', strBegin + 1)
        child = ENTRY_URL + urllib.parse.quote(page[strBegin + 1 : strEnd])
        lastIdx = page.find("<a href=", strEnd)

        crawl(child, cookie)


if __name__ == "__main__":
    authCookie = input("Cookie `auth_token` for the website? ")
    crawl(ENTRY_URL, authCookie)
